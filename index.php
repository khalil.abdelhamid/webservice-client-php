<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion des Enseignants</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 mb-3">
                <h1 class="align-self-center">Ajouter un Enseignant</h1>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-6">
                <form action="index.php" method="post" class="border p-4 rounded-sm">
                    <div class="form-group row">
                        <div class="col">
                            <label for="cin" class="form-label">Cin</label>
                            <input type="text" class="form-control" id="cin" name="cin">
                        </div>
                        <div class="col">
                            <label for="nom" class="form-label">Nom</label>
                            <input type="text" class="form-control" id="nom" name="nom">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="prenom" class="form-label">Prenom</label>
                            <input type="text" class="form-control" id="prenom" name="prenom">
                        </div>
                        <div class="col">
                            <label for="age" class="form-label">Age</label>
                            <input type="number" class="form-control" id="age" name="age">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="Adresse" class="form-label">Adresse</label>
                            <input type="text" class="form-control" id="Adresse" name="adresse">
                        </div>
                        <div class="col">
                            <label for="ville" class="form-label">ville</label>
                            <input type="text" class="form-control" id="ville" name="ville">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="photo" class="form-label">Photo</label>
                            <input type="text" class="form-control" id="photo" name="photo">
                        </div>
                        <div class="col">
                            <label for="sexe" class="form-label">sexe</label>
                            <select id="sexe" class="form-control" NAME="sexe">
                                <option value="M">M</option>
                                <option value="F">F</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group row mt-3">
                        <input type="submit" class="btn btn-primary" name="addProf" value="Ajouter">
                    </div>
                </form>
            </div>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>

</html>

<?php
try {
    $url = "http://localhost:8080/ServerSoapExam/services/ProfService?wsdl";
    $client = new SoapClient($url, array('trace' => 1));
    if ($_POST["addProf"]) {
        $cin =  $_POST["cin"];
        $nom =  $_POST["nom"];
        $prenom =  $_POST["prenom"];
        $age =  $_POST["age"];
        $adresse = $_POST["adresse"];
        $ville = $_POST["ville"];
        $sexe = $_POST["sexe"];
        $photo = $_POST["photo"];

        $prof = array(
            "cin" => $cin,
            "nom" => $nom,
            "prenom" => $prenom,
            "age" => $age,
            "adresse" => $adresse,
            "ville" => $ville,
            "sexe" => $sexe,
            "photo" => $photo
        );

        $objResponse = $client->addProf(array("professeur" => $prof));
         header("location:Enseignant.php", TRUE);
      
    }
} catch (Exception $e) {
    echo "<h2>Exception Error!</h2><br>";
    echo $e->getMessage() . "<br>";
}
