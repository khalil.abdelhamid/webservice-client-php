<?php
try {
    $url = "http://localhost:8080/ServerSoapExam/services/ProfService?wsdl";
    $client = new SoapClient($url, array('trace' => 1));
    $objResponse = $client->getEnseignant();
} catch (Exception $e) {
    echo "<h2>Exception Error!</h2><br>";
    echo $e->getMessage() . "<br>";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion des Modules</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 mb-3">
                <h1 class="align-self-center">Ajouter un Module</h1>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-6">
                <form action="AddModule.php" method="post" class="border p-4 rounded-sm">
                    <div class="form-group row">
                        <div class="col">
                            <label for="cour" class="form-label">Cour</label>
                            <input type="text" class="form-control" id="cour" name="cour">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label for="prof" class="form-label">prof</label>
                            <select id="prof" class="form-control" name="prof">
                                <?php
                                foreach ($objResponse as $prof) {
                                    for ($i = 0; $i < count($prof); $i++) {
                                ?>
                                        <option value=<?= $prof[$i]->cin ?>><?= $prof[$i]->nom ?></option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <input type="submit" class="btn btn-primary" name="addCour" value="Ajouter">
                    </div>
                </form>
            </div>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>

</html>

<?php
try {
    $url2 = "http://localhost:8080/ServerSoapExam/services/ModuleService?wsdl";
    $client2 = new SoapClient($url2, array('trace' => 1));

    if($_POST["addCour"]){
        $matiere = array(
            "cour"=>$_POST["cour"],
            "prof"=>$_POST["prof"]
        );
         $rep  =  $client2->addModule(array("mat" => $matiere));
        print_r($rep);
        //header("location:ListModule.php", TRUE);
    }

} catch (Exception $e) {
    echo "<h2>Exception Error!</h2><br>";
    echo $e->getMessage() . "<br>";
}

?>