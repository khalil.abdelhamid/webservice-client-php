<?php
try {
    $url = "http://localhost:8080/ServerSoapExam/services/ProfService?wsdl";
    $client = new SoapClient($url, array('trace' => 1));
    $objResponse = $client->getEnseignant();
} catch (Exception $e) {
    echo "<h2>Exception Error!</h2><br>";
    echo $e->getMessage() . "<br>";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List des pofesseurs</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="row justify-content-center">
                <div class="col-6 mb-3">
                    <h1 class="align-self-center">Liste des Enseignant</h1>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <table class="table table-smbordered table-lg">
                    <thead>
                        <tr>
                            <th>Cin</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>age</th>
                            <th>adresse</th>
                            <th>ville</th>
                            <th>sexe</th>
                            <td>Modifier</td>
                            <td>Suprimer</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($objResponse as $prof) {
                            for ($i = 0; $i < count($prof); $i++) {
                        ?>
                                <tr>
                                    <td><?= $prof[$i]->cin ?></td>
                                    <td><?= $prof[$i]->nom ?></td>
                                    <td><?= $prof[$i]->prenom ?></td>
                                    <td><?= $prof[$i]->age ?></td>
                                    <td><?= $prof[$i]->adresse ?></td>
                                    <td><?= $prof[$i]->ville ?></td>
                                    <td><?= $prof[$i]->sexe ?></td>
                                    <td>
                                        <a type="button" class="btn btn-primary" href="UpdateEnseignant.php?id=<?= $prof[$i]->cin ?>">Modifier</a>
                                    </td>
                                    <td>
                                        <a type="button" class="btn btn-danger" href="DeleteEnseignant.php?id=<?= $prof[$i]->cin ?>">DELETE</a>
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>

</html>